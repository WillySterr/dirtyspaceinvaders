#include "PlayerInput.h"

bool PlayerInput::Left()
{
    return sf::Keyboard::isKeyPressed(LeftKey);
}

bool PlayerInput::Right()
{
    return sf::Keyboard::isKeyPressed(RightKey);   
}

bool PlayerInput::Fire()
{
    return sf::Keyboard::isKeyPressed(FireKey);
}

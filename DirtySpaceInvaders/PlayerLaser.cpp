#include "PlayerLaser.h"
#include "ConsoleRenderer.h"
#include "PlayField.h"

PlayerLaser::PlayerLaser() : GameObject("PlayerLaser", RS_PlayerLaser)
{
}

PlayerLaser::~PlayerLaser() = default;

void PlayerLaser::Update(PlayField& world)
{
    GameObject::Update(world);
    
    bool deleted = false;
    pos.y -= 1.f;
    
    if (pos.y < 0)
    {
        deleted = true;
    }

    const std::vector<GameObject*> Aliens = world.GetObjects("AlienShip");
    const std::vector<GameObject*> AliensB = world.GetObjects("AlienBShip");


    if(!Aliens.empty())
    {
             
        for (const auto& alien : Aliens)
        {
            if (pos.IntCmp(alien->pos))
            {
                if(alien->DecreaseHealth())
                {
                    deleted = true;
                    world.RemoveObject(alien);
                }
            }
        }
    }else
    {
         if(Aliens.empty() && AliensB.empty())
        {
            //wait 180 for aliens to spawn
            world.SpawnDelay = 3.f;
            world.TimeSinceLastSpawn = 0.f;
            world.IsSpawning = true;
        }else
        {
            for(const auto& alienb : AliensB)
            {
                if (pos.IntCmp(alienb->pos))
                {
                    if(alienb->DecreaseHealth())
                    {
                        deleted = true;
                        world.RemoveObject(alienb);
                    }
                }
            }
        }
    }


    const std::vector<GameObject*> AlienLasers = world.GetObjects("AlienLaser");
    
    if(!AlienLasers.empty())
    {
        for (const auto& laser : AlienLasers)
        {
            if (pos.IntCmp(laser->pos))
            {
                deleted = true;
                world.DespawnLaser(laser);
            }
        }
    }


    const std::vector<GameObject*> Asteroids = world.GetObjects("Asteroid");
    if(!Asteroids.empty()){
        for (const auto& asteroid : Asteroids)
        {
            if (pos.IntCmp(asteroid->pos))
            {
                deleted = true;
            }
        }
    }
        
    if (deleted)
    {
        world.DespawnLaser(this);
    }

    
    
       

}

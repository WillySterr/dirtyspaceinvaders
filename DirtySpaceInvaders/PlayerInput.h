#pragma once
#include <SFML/Window/Keyboard.hpp>

#include "Input.h"

class PlayerInput : public Input
{
public:
    sf::Keyboard::Key LeftKey = sf::Keyboard::Q;
    sf::Keyboard::Key RightKey = sf::Keyboard::D;
    sf::Keyboard::Key FireKey = sf::Keyboard::Space;
    bool Left() override;
    bool Right() override;
    bool Fire() override;
};

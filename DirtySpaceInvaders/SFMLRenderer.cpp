#include "SFMLRenderer.h"

#include "RenderSprite.h"
#include "PlayField.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

SFMLRenderer::SFMLRenderer(Vector2D& screenSize): IRenderer(screenSize), Window(nullptr), ScreenSize(screenSize), Rect(nullptr)
{
    Init();
}

SFMLRenderer::~SFMLRenderer()
{
}

void SFMLRenderer::Update()
{
    sf::Event event;
    while (Window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            Window->close();
    }
}

void SFMLRenderer::Draw()
{
    Window->clear();
    for (auto Object:GetWorld()->GameObjects())
    {
            Rect->setPosition(sf::Vector2f(2200 * Object->pos.x / ScreenSize.x, 900 * Object->pos.y / ScreenSize.y));
            Rect->setFillColor(GetSprite(Object->sprite));
            Window->draw(*Rect);
        
    }
    Window->display();
    
}

void SFMLRenderer::Init()
{
    Window = new sf::RenderWindow(sf::VideoMode(2200, 900), "Dirty Space Invaders");
    Rect = new sf::RectangleShape(sf::Vector2f(2200 / ScreenSize.x, 900 / ScreenSize.y));
    Window->setFramerateLimit(25);
    
}

sf::Color SFMLRenderer::GetSprite(RenderSprite sprite)
{
    switch (sprite)
    {
        case RS_Player:
            return sf::Color::Green;
        case RS_Alien:
            return sf::Color::Red;
        case RS_Asteroid:
            return sf::Color::Yellow;
        case RS_Explosion:
            return sf::Color::White;
        case RS_AlienLaser:
            return sf::Color::Blue;
        case RS_BackgroundTile:
            return sf::Color::Black;
        case RS_BetterAlien:
            return sf::Color::Magenta;
        case RS_PlayerLaser:
            return sf::Color::Cyan;
        default:
            return sf::Color::Black;
        
    }
}

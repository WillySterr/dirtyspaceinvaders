#pragma once
#include "GameObject.h"


class PlayerShip : public GameObject
{
public:
    PlayerShip();
    ~PlayerShip() override;

    void Update(PlayField& world) override;
    void Left();
    void Right(PlayField& world);
    void Fire(PlayField& world);
    

private:
    float health = 1.f;
    bool DecreaseHealth() override;

};

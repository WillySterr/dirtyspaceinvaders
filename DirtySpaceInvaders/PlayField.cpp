#include "PlayField.h"

void PlayField::Update()
{
    if(!gameObjects.empty())
    {
        for (auto* it : gameObjects)
        {
            triedObjects[it->GetObjectType()].push_back(it);
        }
    }

    for (auto* it : gameObjects)
    {
        it->Update(*this);
    }
    
    for (auto it : newGameObjects)
    {
        gameObjects.push_back(it);
    }
    newGameObjects.clear();
    
    // Update list of active objects in the world
    if(!deadGameObjects.empty())
    {
        for (auto i : deadGameObjects)
        {
            auto it = std::find(gameObjects.begin(), gameObjects.end(), i);
            gameObjects.erase(it);
            delete i;
        }
        deadGameObjects.clear();
    }

    triedObjects.clear();
}

GameObject* PlayField::GetPlayerObject()
{
    if(gameObjects.empty())
        return nullptr;

    auto it = std::find_if(gameObjects.begin(), gameObjects.end(), [](GameObject* obj)
    {
        return obj->IsType("PlayerShip");
    });

    if (it != gameObjects.end())
        return *it;

    return nullptr;
}

const std::vector<GameObject*>& PlayField::GetObjects(const char* type)
{
    if(triedObjects.find(type) == triedObjects.end())
    {
        triedObjects.emplace(type, std::vector<GameObject*>());
    }
    return triedObjects.at(type);
}



void PlayField::SpawnLaser(GameObject* newObj)
{
    if (newObj->IsType("AlienLaser"))
        AlienLasers--;

    else if (newObj->IsType("PlayerLaser"))
        PlayerLasers--;

    AddObject(newObj);

}

void PlayField::DespawnLaser(GameObject* newObj)
{
    if (newObj->IsType("AlienLaser"))
    {
        if(AlienLasers < MaxAlienLasers)
        {
            AlienLasers++;
        }
    }else if (newObj->IsType("PlayerLaser"))
    {
        if(PlayerLasers < MaxPlayerLasers)
        {
            PlayerLasers++;
        } 
    }

    RemoveObject(newObj);
    
}

void PlayField::AddObject(GameObject* newObj)
{
    newGameObjects.push_back(newObj);
}

void PlayField::RemoveObject(GameObject* newObj)
{
    for(auto* it : deadGameObjects)
    {
        if (it == newObj)
            return;
    }
    deadGameObjects.push_back(newObj);
}

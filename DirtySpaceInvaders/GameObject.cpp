#include "GameObject.h"

GameObject::GameObject(const std::string &type, RenderSprite sprite)
    : m_objType(type), sprite(sprite)
{
}

const std::string& GameObject::GetObjectType() const
{
    return m_objType;
}

bool GameObject::IsType(const std::string& type) const
{
    return m_objType == type;
}

std::string GameObject::GetObjectType()
{
    return m_objType;
}

void GameObject::Update(PlayField& world)
{
}

bool GameObject::DecreaseHealth()
{
    return true;
}

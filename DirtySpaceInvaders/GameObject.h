#pragma once
#include <string>

#include "RenderSprite.h"
#include "Vector.h"

class PlayField;

class GameObject
{
public:
    GameObject(const std::string& type, RenderSprite sprite);
    virtual ~GameObject() = default;
    std::string m_objType = nullptr;
    Vector2D pos;
    RenderSprite sprite;

    const std::string& GetObjectType() const;
    bool IsType(const std::string& type) const;
    std::string GetObjectType();

    virtual void Update(PlayField& world);
    virtual bool DecreaseHealth();
    
};

#include "Alien.h"
#include "PlayField.h"
#include "AlienLaser.h"

Alien::Alien() : GameObject("AlienShip", RS_Alien)
{
}

Alien::Alien(const std::string& type, RenderSprite sprite): GameObject(type, sprite)
{
}

Alien::~Alien() = default;

bool Alien::DecreaseHealth()
{
    health--;
    if (health <= 0)
    {
        return true;
    }
    return false;
}

void Alien::Update(PlayField& world)
{
    GameObject::Update(world);
    
    // Border check
    if (pos.x < 0 || pos.x >= world.bounds.x - 1)
    {
        direction = -direction;
        pos.y += 1;
    }

    // Border check vertical:
    if (pos.y >= world.bounds.y - 1)
    {
        // kill player
        GameObject* player = world.GetPlayerObject();
        if (pos.IntCmp(player->pos))
        {
            world.RemoveObject(player);
            exit(0);
        }
    }

    pos.x += direction * velocity;

    floatRand fireRate(0, 1);
    if (fireRate(rGen) < 0.5 && world.AlienLasers > 0)
    {
        //Spawn laser
        GameObject* newLaser = new AlienLaser();
        newLaser->pos = {pos.x, pos.y + 1};
        world.SpawnLaser(newLaser);
    }
    
}

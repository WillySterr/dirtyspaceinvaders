#pragma once
#include <random>
#include "GameObject.h"

typedef std::uniform_real_distribution<float> floatRand;


class Alien: public GameObject
{
public:
    Alien();
    Alien(const std::string& type, RenderSprite sprite);
    ~Alien() override;

    virtual bool DecreaseHealth() override;
    virtual void Update(PlayField& world) override;
    
protected:
    float health = 1.f;
    float energy = 0.f;
    float direction = 1.f;
    float velocity = 0.5f;

    
    
};

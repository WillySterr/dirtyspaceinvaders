#include "PlayerShip.h"

#include "ConsoleRenderer.h"
#include "PlayerLaser.h"
#include "PlayField.h"

PlayerShip::PlayerShip() : GameObject("PlayerShip", RS_Player )
{
}

PlayerShip::~PlayerShip()
{
}

void PlayerShip::Update(PlayField& world)
{
    GameObject::Update(world);
    
    if (world.InputController->Left())
        Left();
    else if (world.InputController->Right())
        Right(world);
    
    if (world.InputController->Fire())
    {
        Fire(world);
    }
    
}

void PlayerShip::Left()
{
    if(pos.x >= 1)
    {
        pos.x -= 1;
    }
}

void PlayerShip::Right(PlayField& world)
{
    if(pos.x < world.bounds.x - 1)
    {
        pos.x += 1;
    }
}

void PlayerShip::Fire(PlayField& world)
{
    if(world.PlayerLasers > 0)
    {
        //Spawn laser
        GameObject* newLaser = new PlayerLaser();
        newLaser->pos = {pos.x, pos.y - 1};
        world.SpawnLaser(newLaser);
    }
}

bool PlayerShip::DecreaseHealth()
{
    health--;
    if (health <= 0)
    {
        return true;
    }
    return false;
}

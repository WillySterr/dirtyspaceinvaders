#pragma once
#include <map>
#include <vector>

#include "GameObject.h"
#include "Input.h"
#include "Vector.h"

class GameObject;

class PlayField
{
private:
    std::vector<GameObject*> gameObjects;
    std::vector<GameObject*> newGameObjects;
    std::vector<GameObject*> deadGameObjects;

    std::map<std::string, std::vector<GameObject*>> triedObjects;

public:
    Input* InputController;
    Vector2D bounds;

    // Number of available active laser slots for aliens and player
    int AlienLasers = 10;
    int PlayerLasers = 4;
    int MaxPlayerLasers = 4;
    int MaxAlienLasers = 10;
    bool IsSpawning = false;
    float TimeSinceLastSpawn = 0.f;
    float SpawnDelay = 180.f;
    int AlienAToSpawn = 4;
    int AlienBToSpawn = 1;
    int AsteroidToSpawn = 10;

    PlayField(Vector2D iBounds) : InputController(nullptr), bounds(iBounds) {}
    
    const std::vector<GameObject*>& GameObjects() { return gameObjects; }

    void Update();

    GameObject* GetPlayerObject();
    
    const std::vector<GameObject*>& GetObjects(const char* type);

    void SpawnLaser(GameObject* newObj);
    
    void DespawnLaser(GameObject* newObj);
    
    void AddObject(GameObject* newObj);

    void RemoveObject(GameObject* newObj);
    
public:
    
};

PlayField* GetWorld();
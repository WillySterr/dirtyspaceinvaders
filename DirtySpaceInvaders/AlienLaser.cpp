#include "AlienLaser.h"

#include <vector>

#include "PlayField.h"

AlienLaser::AlienLaser() : GameObject("AlienLaser", RS_AlienLaser)
{
}

AlienLaser::~AlienLaser()
{
}

void AlienLaser::Update(PlayField& world)
{
    GameObject::Update(world);
    
    bool deleted = false;
    pos.y += 1.f;
    if (pos.y > world.bounds.y)
    {
        deleted = true;
    }

    GameObject* player = world.GetPlayerObject();


    std::vector<GameObject*> PlayerLasers = world.GetObjects("PlayerLaser");
    
    
    if(player == nullptr)
        return;
    
    if (pos.IntCmp(player->pos))
    {
        deleted = true;
        if(player->DecreaseHealth()){
            world.RemoveObject(player);
            exit(0);
        }
                
    }

    if(!PlayerLasers.empty())
    {
        for(auto& laser : PlayerLasers)
        {
            if (pos.IntCmp(laser->pos))
            {
                deleted = true;
                world.DespawnLaser(laser);
                break;
            }
        }
    }

    std::vector<GameObject*> Asteroids = world.GetObjects("Asteroid");

    if(!Asteroids.empty())
    {
        for(auto& asteroid : Asteroids)
        {
            if (pos.IntCmp(asteroid->pos))
            {
                deleted = true;
                break;
            }
        }
    }
    
    if (deleted)
    {
        world.DespawnLaser(this);
    }
    
}

#pragma once

#include "Renderer.h"
#include <vector>

#include "RenderSprite.h"

struct RenderItem
{
	RenderItem(const Vector2D& iPos, char iSprite) : pos(iPos), sprite(iSprite) {}
	Vector2D pos;
	char sprite;
};
typedef std::vector<RenderItem> RenderItemList;

class Renderer : public IRenderer
{
public:
	Renderer(const Vector2D& bounds);
	~Renderer() override;

	// Draws all game objects after clearing filling the Canvas with _ symbol
	void Update(const RenderItemList& renderList);
	void Update() override;

	void Draw() override;
	
private:
	Vector2D renderBounds;
	int curIdx = 0;
	struct
	{
		unsigned char* canvas = nullptr;
	} disp[2]; // double buffer our canvas for no flicker display

	int canvasSize = 0;
	unsigned char* CurCanvas(int x, int y);

	// Fills whole canvas array with sprite
	void FillCanvas(unsigned sprite);

	unsigned GetSprite(RenderSprite sprite);
};


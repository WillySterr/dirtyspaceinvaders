#pragma once

#include "Renderer.h"
#include "RenderSprite.h"
#include <SFML/Graphics/Color.hpp>


namespace sf
{
    class RenderWindow;
    class RectangleShape;
}

class SFMLRenderer final : public IRenderer
{
public:
    SFMLRenderer(Vector2D& screenSize);
    ~SFMLRenderer() override;

    void Update() override;
    void Draw() override;

    void Init();
    sf::Color GetSprite(RenderSprite sprite);

private:
    sf::RenderWindow* Window;
    Vector2D ScreenSize;
    sf::RectangleShape* Rect;
    
};

#include <vector>
#include <iostream>

#include "ConsoleRenderer.h"

#include <Windows.h>

#include "PlayField.h"
#include "RenderSprite.h"

void setCursorPosition(int x, int y)
{
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	std::cout.flush();
	const COORD coord = { static_cast<SHORT>(x), static_cast<SHORT>(y) };
	SetConsoleCursorPosition(hOut, coord);
}

Renderer::Renderer(const Vector2D& bounds)
	: IRenderer(bounds), renderBounds(bounds)
{
	canvasSize = static_cast<int>(bounds.x * bounds.y);
	disp[0].canvas = new unsigned char[canvasSize];
	disp[1].canvas = new unsigned char[canvasSize];
}


Renderer::~Renderer()
{
	delete[] disp[0].canvas;
	delete[] disp[1].canvas;
}

void Renderer::Update(const RenderItemList& RenderList)
{
	FillCanvas(' ');

	for (auto ri : RenderList)
	{
		int x = static_cast<int>(ri.pos.x);
		int y = static_cast<int>(ri.pos.y);

		if (x >= 0 && x < renderBounds.x && y >= 0 && y < renderBounds.y)
		{
			*CurCanvas(static_cast<int>(ri.pos.x), +static_cast<int>(ri.pos.y)) = ri.sprite;
		}
	}
}

void Renderer::Update()
{
	
	RenderItemList rl;
	for (auto* it : GetWorld()->GameObjects())
	{
		RenderItem a = RenderItem(Vector2D(it->pos), GetSprite(it->sprite));
		rl.push_back(a);
	}

	Update(rl);
}

void Renderer::Draw()
{
	for (int y = 0; y < renderBounds.y; y++)
	{
		for (int x = 0; x < renderBounds.x; x++)
		{
			setCursorPosition(x, y);
			std::cout << *CurCanvas(x, y);
		}
		std::cout << std::endl;
	}

	curIdx++;
}

unsigned char* Renderer::CurCanvas(int x, int y)
{
	return &disp[curIdx % 2].canvas[x + static_cast<int>(renderBounds.x) * y];
}

void Renderer::FillCanvas(unsigned sprite)
{
	for (int i = 0; i < canvasSize; i++)
	{
		*CurCanvas(i, 0) = sprite;
	}
}

unsigned Renderer::GetSprite(RenderSprite sprite)
{
	switch (sprite)
	{
		case RS_BackgroundTile:
			return ' ';
		case RS_Alien:
			return 'A';
		case RS_BetterAlien:
			return 'B';
		case RS_Player:
			return 'P';
		case RS_PlayerLaser:
			return '&';
		case RS_AlienLaser:
			return '|';
		case RS_Explosion:
			return '*';
		case RS_Asteroid:
			return 'o';
	
	}
	return 0;
}


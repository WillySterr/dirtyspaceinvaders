#pragma once

#include "Vector.h"

class IRenderer
{
public:
    IRenderer(const Vector2D& screenSize);
    virtual ~IRenderer() = default;

    virtual void Update() = 0;
    virtual void Draw() = 0;
};

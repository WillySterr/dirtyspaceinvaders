#include <random>
#include <thread>
#include <memory>
#include <Windows.h>

#include "Alien.h"
#include "PlayerShip.h"
#include "PlayField.h"
#include "RndInput.h"
#include "AlienB.h"
#include "Asteroid.h"
#include "PlayerInput.h"
#include <SFML/System/Clock.hpp>

#define SFML_RENDERER true

#if SFML_RENDERER == true
#include "SFMLRenderer.h"
#else
#include "ConsoleRenderer.h"
#endif

typedef std::uniform_int_distribution<int> intRand;
typedef std::uniform_real_distribution<float> floatRand;

PlayField* world;
PlayField* GetWorld()
{
	return world;
}
int main()
{
	
	rGen.seed(1);
	
	Vector2D size(80, 28);

#if SFML_RENDERER == true
	SFMLRenderer consoleRenderer(size);
#else
	Renderer consoleRenderer(size);
#endif	
	world = new PlayField(size);
		
	//RndInput rndInput;
	//world.InputController = &rndInput;
	PlayerInput playerInput;
	world->InputController = &playerInput;
	
	intRand xCoord(0, static_cast<int>(size.x)- 1);
	intRand yCoord(0, 10);
	
	// Populate aliens
	for (int k = 0; k < world->AlienAToSpawn; k++)
	{
		Alien* a = new Alien();
		a->pos.x = static_cast<float>(xCoord(rGen));
		a->pos.y = static_cast<float>(yCoord(rGen));
		world->AddObject(a);
	}

	// Populate Asteroids
	for (int k = 0; k < world->AsteroidToSpawn; k++)
	{
		Asteroid* ast = new Asteroid();
		ast->pos.x = static_cast<float>(xCoord(rGen));
		ast->pos.y = static_cast<float>(yCoord(rGen));
		world->AddObject(ast);
	}
	
	// Add player
	PlayerShip* p = new PlayerShip();
	p->pos = Vector2D(40, 27);
	world->AddObject(p);
	
	// Add alien b
	std::vector<AlienB*> AlienBList;
	for(int i=0; i<world->AlienBToSpawn; i++)
	{
		AlienB* b = new AlienB();
		b->pos.x = static_cast<float>(xCoord(rGen));
		b->pos.y = static_cast<float>(yCoord(rGen));
		AlienBList.push_back(b);
	}
	sf::Clock clock;
		
	while (true)
	{
		world->Update();
		consoleRenderer.Update();
		consoleRenderer.Draw();
	
		// Time since last update
		float deltaTime = clock.restart().asSeconds();

		//if time since last spawn is greater than spawn delay and is spawning is true
		if(world->IsSpawning)
		{
			if(world->TimeSinceLastSpawn >= world->SpawnDelay)
			{
				for(auto& b : AlienBList)
				{
					world->AddObject(b);
				}
				world->IsSpawning = false;
				world->TimeSinceLastSpawn = 0.f;
			}
			else
			{
				world->TimeSinceLastSpawn += deltaTime;
			}
		}

		// Sleep a bit so updates don't run too fast
		//std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	return 0;
}

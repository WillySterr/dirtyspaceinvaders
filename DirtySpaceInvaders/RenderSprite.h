#pragma once

enum RenderSprite
{
    RS_BackgroundTile,
    RS_Player,
    RS_Alien,
    RS_BetterAlien,
    RS_PlayerLaser,
    RS_AlienLaser,
    RS_Explosion,
    RS_Asteroid
};

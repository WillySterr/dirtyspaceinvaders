# DirtySpaceInvaders



## Treshold 0

- [x] Ajouts des compléments const et override sur les classes héritées
- [x] Changement de la classe Game en Singleton
- [x] Retrait des parties redondantes dans le code et changement des casts en version c++
- [x] Changement des pointeurs en références
- [x] Ajout de verifications sur les pointeurs
- [x] Ajout des destructeurs manquants et des destructeurs virtuels
- [x] Ajout des collisions entre les projectiles et ceux des ennemis
- [x] Ajout des collisions entre les projectiles et le joueur
- [x] Ajout de fonction pour recuperer les gameobjects par leur type
- [x] Ajout de la gestion des collisions du treshold 0
- [x] Fait des choses qui ont fait des trucs

## Treshold 1

- [x] Changement du type dans le constructeur de la classe GameObject pour un type string
- [x] Ajout des inputs sfml
- [x] Ajout du rendu sfml
- [x] Transfert dans un enum des types de gameobject
- [x] Le system de rendu gère maintenant de lui meme l'afichage des gameobjects
- [x] Ajout des asteroids
- [x] Ajout d'un timer pour le spawn des aliens B
- [x] Changement du systeme de stockage des gameobjects pour un systeme de map
- [x] Fait des choses qui ont fait des trucs

## Treshold 2

- [x] Ajout d'un define dans le main pour le choix du rendu


